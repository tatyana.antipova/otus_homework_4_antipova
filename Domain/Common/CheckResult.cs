﻿namespace Domain.Settings;

public enum CheckResult
{
    Win,
    NumberGreater,
    NumberLower,
    Failure
}
