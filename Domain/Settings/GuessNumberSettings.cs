﻿namespace Domain.Settings;

public class GuessNumberSettings
{
    public int TryCount { get; set; }
    public int MinValue { get; set; }
    public int MaxValue { get; set; }
}
