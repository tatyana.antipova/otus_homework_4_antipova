﻿using Application.Abstractions;
using Domain.Settings;
using Microsoft.Extensions.Options;

namespace Application.Services
{
    public class GeneratorService : IGenerator
    {
        private readonly GuessNumberSettings _settings;
        private readonly Random _rnd;

        public GeneratorService(IOptions<GuessNumberSettings> config)
        {
            _settings = config.Value;
            _rnd = new Random();
        }
        public int GetNumber()
        {
            return _rnd.Next(_settings.MinValue, _settings.MaxValue);
        }
    }
}
