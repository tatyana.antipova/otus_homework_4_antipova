﻿using Application.Models;

namespace Application.Services;

public static class NumberComparerLocator
{
    public static NumberComparerBase GetComparer(string userInput)
    {
        if (Int32.TryParse(userInput, out int parsedInt))
        {
            return new NumberComparerInteger(parsedInt);
        }
        else if (decimal.TryParse(userInput, out decimal parsedDec))
        {
            return new NumberComparerDecimal(parsedDec);
        }
        return null;
    }
}
