﻿using Domain.Settings;

namespace Application.Models;

public class NumberComparerDecimal : NumberComparerBase
{
    public NumberComparerDecimal(decimal input)
    {
        Input = input;
    }

    public override CheckResult Compare(int randomNumber)
    { 
        return base.Compare(randomNumber);
    }
}
