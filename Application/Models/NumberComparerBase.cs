﻿using Domain.Settings;

namespace Application.Models;

public abstract class NumberComparerBase
{
    public decimal Input { get; set; }

    public virtual CheckResult Compare(int randomNumber)
    {
        if (Input == randomNumber)
        {
            return CheckResult.Win;
        }
        else if (Input > randomNumber)
        {
            return CheckResult.NumberGreater;
        }
        else
        {
            return CheckResult.NumberLower;
        }
    }
}
