﻿using Domain.Settings;

namespace Application.Models;

public class NumberComparerInteger : NumberComparerBase
{
    public NumberComparerInteger(int input)
    {
        Input = (decimal)input;
    }

    public override CheckResult Compare(int randomNumber)
    { 
        return base.Compare(randomNumber);
    }
}
