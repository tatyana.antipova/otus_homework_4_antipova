﻿using Application.Abstractions;
using Application.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Application;

public static class ServiceRegistration
{
    public static void AddApplicationServices(this IServiceCollection services, IConfiguration Configuration)
    {
        services.AddSingleton<IGenerator, GeneratorService>();     
    }
}