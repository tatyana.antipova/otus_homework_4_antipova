﻿using Application.Abstractions;
using Domain.Settings;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Application.Presentation;

internal class Program
{
    static void Main(string[] args)
    {
        IServiceCollection services = new ServiceCollection();

        Startup startup = new Startup();
        startup.ConfigureServices(services);

        IServiceProvider serviceProvider = services.BuildServiceProvider();


        var gameSettings = serviceProvider.GetService<IOptions<GuessNumberSettings>>().Value;
        var generatorService = serviceProvider.GetRequiredService<IGenerator>();

        var game = new GuessNumberGame(gameSettings, generatorService);

        while (true)
        {
            game.Run();
        }
    }
}