﻿using Application.Abstractions;
using Application.Services;
using Domain.Settings;

namespace Application.Presentation;

internal class GuessNumberGame
{
    private readonly GuessNumberSettings _settings;
    private readonly IGenerator _generator;
    public GuessNumberGame(GuessNumberSettings settings, IGenerator generator)
    {
        _settings = settings;
        _generator = generator;
    }
    public void Run()
    {
        Console.WriteLine(GetStartWording());

        var generated = _generator.GetNumber();
        int tried = 0;


        while (tried++ < _settings.TryCount)
        {
            Console.Write("Ваше число: ");
            string userInput = Console.ReadLine();

            var result = NumberComparerLocator.GetComparer(userInput)?
                                                   .Compare(generated);

            Console.WriteLine(GetTryResultWording(result, tried));

            if (result is null || new[] { CheckResult.Win, CheckResult.Failure }.Any(x => x.Equals(result)))
                return;
        }

        Console.WriteLine($"Вы использовали {_settings.TryCount} из {_settings.TryCount} попыток. Завершение игры.");
    }

    private string GetTryResultWording(CheckResult? result, int tried) =>
        result switch
        {
            CheckResult.Win => $"Поздравляем, вы прошли игру. Использовано {tried} попыток.",
            CheckResult.NumberGreater => $"Ваше число больше чем загаданное. Ипользовано {tried} из {_settings.TryCount} попыток.",
            CheckResult.NumberLower => $"Ваше число меньше чем загаданное. Ипользовано {tried} из {_settings.TryCount} попыток.",
            CheckResult.Failure or _ => "Неверный ввод. Завершение игры."
        };

    private string GetStartWording() => @$"
    ###################################################
    Нужно отгадать число от {_settings.MinValue} до {_settings.MaxValue}.
    Количество попыток: {_settings.TryCount}.
    Чтобы начать сначала, введите любой не-числовой символ.
    ";


}
